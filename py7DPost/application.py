# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 21:34:30 2019

@author: rouxemi
"""

import py7DPost.core as py7D
import pandas as pd
import numpy as np

class Post_traction:
    def __init__(self, root_name):
        # =====================================================================
        # Read Meta data
        # =====================================================================
        print('Process ' + root_name)
        df = pd.read_csv(root_name ,
                         error_bad_lines=False,
                         warn_bad_lines=False,
                         index_col=0)
        
        df = pd.to_numeric(df.val, errors='ignore')
        nblines2skip = df.size + 1
        self.metadata = df.T
        self.metadata.test_type = self.metadata.test_type.replace(" ", "")
        
        if self.metadata.test_type == 'Traction':
            self.S = pd.to_numeric(self.metadata.ep) * pd.to_numeric(self.metadata.lar)
        elif self.metadata.test_type == 'Compression':
            self.S = pd.to_numeric(self.metadata.diam) **2*np.pi/4
        else:
            self.S = 0.
        
        
        print('test_type=', self.metadata.test_type)    
        print('S=', self.S)

        # =====================================================================
        # Read Force data
        # =====================================================================
        self.df = pd.read_csv(root_name ,
                              skiprows=nblines2skip, index_col=0)

        # =====================================================================
        # Read 7D Data
        # =====================================================================
        self.data7D = py7D.read7DData(mat_path=root_name[:-4]+'.mat')
    
    def process(self):
        self.extenso()
        self.stress()
        self.lankford_coef()

    def extenso(self):
        # =====================================================================
        # Extenso X dir       
        # =====================================================================
        sy, sx = self.data7D['X'].shape
        p1 = (0, int((sy-1) / 2))
        p2 = (sx-1, int((sy-1) / 2))
        print(p1)
        print(p2)
        self.df['ext_t0'] = py7D.extenso(p1, p2,
                                           self.data7D,
                                           trueStrain = False)
        self.df['ext_t'] = py7D.extenso(p1, p2,
                                           self.data7D,
                                           trueStrain = True)
        # =====================================================================
        # Extenso Y dir       
        # =====================================================================        
        p1 = (int(sx/2), int(.2*sy))
        p2 = (int(sx/2), int(.9*sy))
        self.df['ext_l0'] = py7D.extenso(p1, p2, self.data7D,
                                           trueStrain = False)
        self.df['ext_l'] = py7D.extenso(p1, p2, self.data7D,
                                           trueStrain = True)
    def stress(self):
        self.df['syy'] =  (1 + self.df['ext_l0'])  * self.df.Force_N / self.S 
        
    def lankford_coef(self):
        df=self.df
        #self.df['lankford_coef'] = (-df.ext_l-df.ext_t)/df.ext_t
        self.df['lankford_coef'] = df.ext_t / (-df.ext_l - df.ext_t)
        
        
