# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 14:24:28 2019

@author: rouxemi
"""
import scipy.io
import numpy as np
import pandas as pd

NB_IMG_MAX = 1000


def read7DData(mat_path = None, keys = ('dx','dy', 'exx', 'eyy', 'exy')):

    ## LECTURE des resulats 7D exportés en format matlab V4
    mat = scipy.io.loadmat(mat_path)
    
    out={}
    
    if 'pixel_to_mm' in mat:
        pix2mm = mat["pixel_to_mm"][0,0]
    else:
        pix2mm = 1    
    out['pix2mm'] = pix2mm
    
    # Initial grid coordinates
    out['X']  = mat["x"]
    out['Y']  = mat["y"]
    
  
    for k in keys:
        out[k]=_extractField(mat, k)
    
    # dispalcement for each point at each inc
    #dx=_extractField(mat, 'dx')
    #dy=_extractField(mat, 'dy')
    
    # Strain
    #exx = _extractField(mat, 'exx')
    #eyy = _extractField(mat, 'eyy')
 
    out['norm_d'] = np.sqrt(out['dx']**2.+out['dy']**2)
    return out



def _extractField(mat, name):
    key = name+"{:03}".format(1)
    if key in mat:
        X  = mat[key]
    
        field = np.zeros((NB_IMG_MAX, X.shape[0], X.shape[1]))
        #for i in num_img.astype(np.uint32):
        for i in range(1,NB_IMG_MAX):
            key = name+"{:03}".format(i)
            if key in mat:
                field[i] = mat[key]
            else:
                break
        return field[:i]
    else:
        print('no data named ' + name + ' in 7D mat file.' )
        return None
    

def extenso(p1, p2, data, trueStrain=True):
    X = data['X']
    Y = data['Y']
    dx = data['dx']
    dy = data['dy']

    X1 = np.array([X[p1[::-1]], Y[p1[::-1]]])
    X2 = np.array([X[p2[::-1]], Y[p2[::-1]]])

    dX1 = np.array([dx[:, p1[1], p1[0]], dy[:, p1[1], p1[0]]])
    dX2 = np.array([dx[:, p2[1], p2[0]], dy[:, p2[1], p2[0]]])
    
    X1 = (X1 + dX1.T).T
    X2 = (X2 + dX2.T).T
    dX = np.linalg.norm(X1-X2, axis=0)
    ext = (dX-dX[0])/dX[0]
    
    if trueStrain:
        ext = np.log(1 + ext)
    return ext
