# -*- coding: utf-8 -*-
"""
Created on Sat Dec  7 14:38:10 2019

@author: rouxemi
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import pandas as pd


import scipy.io

from scipy.optimize import minimize
import numpy.polynomial.polynomial as poly
from mpl_toolkits.mplot3d import Axes3D 
import py7DPost.core as py7D
from py7DPost.application import Post_traction

plt.close('all')

# =============================================================================
# Process
# =============================================================================
root_name = 'Trac1.csv'

PT = Post_traction(root_name)
PT.process()

# Read 7D Data
data=PT.data7D
df=PT.df
metadata =PT.metadata 
S= PT.S
# convert dictionary data to variable
for key,val in data.items(): exec(key + '=val')

# =============================================================================
# Plot Graph
# =============================================================================
 
plt.figure()
plt.plot(df['ext_l'],'o', label='Longi')
plt.plot(df['ext_t'],'+', label='Transversal')
plt.xlabel('Inc')
plt.ylabel('[mm]')
plt.legend()

plt.figure()
plt.plot(df.lankford_coef,'o')
plt.xlabel('Inc')
plt.ylabel('lankfoard coeff.')
plt.grid()

plt.figure()
plt.plot(df.ext_l, df.syy, '-o')
plt.xlabel('$\epsilon$')
plt.ylabel('$\sigma [MPa]$')
plt.grid()

# =============================================================================
# Plot Field like 7D
# =============================================================================
inc = 35
img=plt.imread(".\img\\"+root_name[:-4] + "_{:04}.tif".format(inc))[::-1]
plt.figure()
s=1
plt.imshow(img, cmap='gray', origin = 'lower')
plt.quiver(X[::s]+dx[inc][::s],
           Y[::s]+dy[inc][::s],
           dx[inc][::s],
           dy[inc][::s], 
           norm_d[inc][::s],
           units='xy',
           cmap='jet')
plt.axis('off')
plt.colorbar()


