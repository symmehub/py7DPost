# -*- coding: utf-8 -*-
"""
Created on Sat Dec  7 14:38:10 2019

@author: rouxemi
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import pandas as pd


import scipy.io

from scipy.optimize import minimize
import numpy.polynomial.polynomial as poly
from mpl_toolkits.mplot3d import Axes3D 
import py7DPost.core as py7D
from py7DPost.application import Post_traction

plt.close('all')

# =============================================================================
# Process
# =============================================================================
root_name = 'Trac1'

PT = Post_traction(root_name)
PT.extenso()
PT.stress()
PT.lankford_coef()

data=PT.data7D
df=PT.df
metadata =PT.metadata 
S= PT.S


# =============================================================================
# Plot
# =============================================================================
 
plt.figure()
plt.plot(df['ext_l'],'o')
plt.plot(df['ext_t'],'+')


plt.figure()
plt.plot(df.lankford_coef,'o')
plt.grid()

plt.figure()
plt.plot(df.ext_l, df.syy, '-o')
plt.grid()