# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 11:20:18 2019

@author: rouxemi
"""


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import pandas as pd


import scipy.io

from scipy.optimize import minimize
import numpy.polynomial.polynomial as poly
from mpl_toolkits.mplot3d import Axes3D 
from mpl_toolkits.axes_grid.axislines import SubplotZero
from matplotlib.transforms import BlendedGenericTransform
from py7DPost.application import Post_traction

plt.close('all')

root_name = 'Bend1.csv'
Bend=Post_traction(root_name)

# Read 7D Data
data = Bend.data7D
df= Bend.df
# convert dictionary data to variable
for key,val in data.items(): exec(key + '=val')

# PLOT FIGURE like 7D
inc = 37
img=plt.imread(root_name[:-4] + "_{:04}.tif".format(inc))[::-1]
plt.figure()
s=1
plt.imshow(img, cmap='gray', origin = 'lower')
plt.quiver(X[::s]+dx[inc][::s],
           Y[::s]+dy[inc][::s],
           dx[inc][::s],
           dy[inc][::s], 
           norm_d[inc][::s],
           units='xy',
           cmap='jet')
plt.axis('off')
plt.colorbar()

######################
levels = np.linspace(-.1, .1, 21)
fig, ax = plt.subplots()
ax.imshow(img[:,:,0], cmap='gray', origin = 'lower')
cf=ax.contourf(X+dx[inc],
             Y+dy[inc],
             exx[inc],
             levels,
             cmap='RdYlBu_r',
             alpha = 0.6,
             )

plt.colorbar(cf)
levels = np.linspace(-.04, .04, 5)
CS=ax.contour(X+dx[inc],
              Y+dy[inc],
              exx[inc],
              levels, cmap = 'RdYlBu_r', linestyles = None)

ax.clabel(CS, inline=1, fontsize=10)
ax.axis('off')
ax.set_title("$\epsilon_{xx}$")
plt.savefig('bend.pdf', dpi = 600)



