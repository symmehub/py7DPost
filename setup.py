from setuptools import setup, find_packages

setup(
    name='pypost7D',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'pypost7D',
    ],
    author='Your Name',
    author_email='your.email@example.com',
    description='A description of your module',
    url='https://gitlab.com/symmehub/py7DPost',
)